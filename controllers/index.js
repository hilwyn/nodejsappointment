const db = require("../models");
const Slot = require("../models/slot");
const Nexmo = require("nexmo");

const Appointment = require("../models/appointment");
const { request } = require("express");

module.exports = {
  all: function (req, res) {
    // Returns all appointments
    db.Appointment.find({}).exec((err, appointments) => res.json(appointments));
  },
  create: function (req, res) {
    var requestBody = req.body;
    var newslot = new Slot({
      slot_time: requestBody.slot_time,
      slot_date: requestBody.slot_date,
      created_at: Date.now(),
    });
    newslot.save();
    // Creates a new record from a submitted form
    var newappointment = new Appointment({
      first_name: requestBody.firstName,
      last_name: requestBody.lastName,
      gender: requestBody.gender,
      middle_initial: requestBody.middle_initial,
      date_of_birth:  requestBody.date_of_birth,
      email: requestBody.email,
      phone: requestBody.phone,
      test_type: requestBody.test_type,
      slot_date: requestBody.slot_date,
      slot_date_string: requestBody.slot_date_string,
      slot_time: requestBody.slot_time,
      slot_time_string: requestBody.slot_time_string,
      id_number: requestBody.id_nunber,
      slots: newslot._id,
    });

    console.log(newappointment);

    const nexmo = new Nexmo({
      apiKey: "1f1690f7",
      apiSecret: "MNX1LpJPSD9J1P6H",
    });
    let msg =
      requestBody.first_name +
      " your appointment is confirmed for " +  requestBody.appointment_date_string + " at " + requestBody.slot_time_string;
      // and saves the record to
      // the data base
      newappointment.save((err, saved) => {
      // Returns the saved appointment
      // after a successful save
      db.Appointment.find({ _id: saved._id })
        .populate("slots")
        .exec((err, appointment) => res.json(appointment));
      const from = "15404465104";
      const to = "19193904006";
      nexmo.message.sendSms(from, to, msg, (err, responseData) => {
        if (err) {
          console.log(err);
        } else {
          console.dir(responseData);
        }
      });
    });
  },
};
