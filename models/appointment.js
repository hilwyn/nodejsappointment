const mongoose = require("mongoose");
const Schema = mongoose.Schema,
  model = mongoose.model.bind(mongoose),
  ObjectId = mongoose.Schema.Types.ObjectId;
const appointmentSchema = new Schema({
  id: ObjectId,
  first_name: String,
  middle_initial: String,
  last_name: String,
  date_of_birth: String,
  test_type: String,
  test_type_name: String,
  id_number: String,
  slot_date: String,
  slot_date_string: String,
  slot_time: String,
  slot_time_string: String,
  gender: String,
  email: String,
  phone: String,
  slots: { type: ObjectId, ref: "Slot" },
  created_at: Date,
});
const Appointment = model("Appointment", appointmentSchema);

module.exports = Appointment;
